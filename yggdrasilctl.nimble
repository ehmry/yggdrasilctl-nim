# Package

version       = "0.1.0"
author        = "Emery Hemingway"
description   = "An example of communicating with the Yggdrasil control socket over Syndicate"
license       = "Unlicense"
srcDir        = "src"
bin           = @["yggdrasilctl"]


# Dependencies

requires "nim >= 1.6.4"
